package com.example.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PessoaFactory {

	public List<Pessoa> list(){
		List<Pessoa> result = new ArrayList<>();
		
		result.add(new Pessoa("Joao da Silva",    "323.121.123-00", new Date()));
		result.add(new Pessoa("Marco Antonio",    "324.121.123-00", new Date()));
		result.add(new Pessoa("Claudio Alves",    "325.121.123-00", new Date()));
		result.add(new Pessoa("Carolina Ribeiro", "326.121.123-00", new Date()));
		result.add(new Pessoa("Maria das Dores",  "327.121.123-00", new Date()));
		
		return result;
	}
	
}
